package com.cola.colaboot.config.security;

import com.cola.colaboot.config.exception.ColaException;
import com.cola.colaboot.config.exception.LoginException;
import com.cola.colaboot.module.system.pojo.SysUser;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtils {

    public static SysUser currentUser(){
        try{
            return (SysUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        }catch (Exception e){
            throw new LoginException("用户未登录");
        }
    }
}
