package com.cola.colaboot.module.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cola.colaboot.module.system.pojo.SysRole;

public interface SysRoleService extends IService<SysRole> {
    IPage<SysRole> pageList(SysRole sysRole, Integer pageNo, Integer pageSize);
}
