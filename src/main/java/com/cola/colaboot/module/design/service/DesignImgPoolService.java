package com.cola.colaboot.module.design.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cola.colaboot.module.design.pojo.DesignImgPool;

public interface DesignImgPoolService extends IService<DesignImgPool> {
    IPage<DesignImgPool> pageList(DesignImgPool pool, Integer pageNo, Integer pageSize);
}
